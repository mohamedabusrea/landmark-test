(function() {
    validate('#loginForm');
})();

function validate(formID) {
    let formInputs = $(formID).find('input');

    formInputs.each(function() {
        $(this).on('keyup blur', function() {
            /* WHEN INPUT IS EMPTY */
            if (this.required && this.value.length <= 0) {
                $(this).siblings().removeClass('box__validation--active')
                $(this).siblings('.box__validation--invalid').addClass('box__validation--active')
            }
            /* WHEN INPUT IS FILLED */
            else {
                $(this).siblings().removeClass('box__validation--active')
                $(this).siblings('.box__validation--valid').addClass('box__validation--active')

                /* INPUT IS VALID EMAIL */
                if ($(this).attr('type') == 'email') {
                    if (validateEmail(this.value)) {
                        $(this).siblings().removeClass('box__validation--active')
                        $(this).siblings('.box__validation--valid').addClass('box__validation--active')

                        if ($(this).val() == 'test@test.com') {
                            $(this).siblings().removeClass('box__validation--active')
                            $(this).siblings('.box__validation--invalidEmailFound').addClass('box__validation--active')
                        }
                    }
                    /* INPUT IS INVALID EMAIL */
                    else {
                        $(this).siblings().removeClass('box__validation--active')
                        $(this).siblings('.box__validation--invalidEmail').addClass('box__validation--active')
                    }
                }
                if ($(this).attr('matchTo')) {
                    let target = $(this).attr('matchTo');
                    if ($(this).val() == $(target).val()) {
                        $('#registerForm [matchTo]').siblings('.box__validation--active').removeClass('box__validation--active')
                        $('#registerForm [matchTo]').siblings('.box__validation--valid').addClass('box__validation--active')
                    }
                    else {
                        $('#registerForm [matchTo]').siblings('.box__validation--active').removeClass('box__validation--active')
                        $('#registerForm [matchTo]').siblings('.box__validation--invalidPass').addClass('box__validation--active')
                    }
                }
            }
            /* VALIDATE SUBMIT BUTTON */
            validateButton(formID);
        })
    })
    /* VALIDATE SELECT */
    $('#gender').on('change', function() {
        $(this).siblings().removeClass('box__validation--active')
        $(this).siblings('.box__validation--valid').addClass('box__validation--active')
        validateButton(formID);
    })
}

function validateButton(formID) {
    let formInputs = $(formID).find('[required]');

    let check = Array.from(formInputs).every(function(e) {
        return $(e).siblings('.box__validation--valid').hasClass('box__validation--active')
    });
    if (check) {
        $(formID).find('.button').removeClass('button--invalid')
        $(formID).find('.button').removeAttr('disabled')
    }
    else {
        $(formID).find('.button').addClass('button--invalid').attr('disabled', 'true')
    }
}

function validateEmail(email) {
    let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function openVideo() {
    $('.popup').addClass('popup--active')
}

function closeVideo() {
    event.stopPropagation();
    let temp = $('#youtube').attr('src');
    $('#youtube').attr('src', '');
    $('#youtube').attr('src', temp);
    $('.popup').removeClass('popup--active')
}

function register() {
    let registerForm = `<form id="registerForm">
                        <!-- EMAIL -->
                        <div class="box__input">
                            <input class="default-input" type="email" name="email" placeholder="Email Address" required>
                            <div class="box__validation box__validation--invalid">
                                <img class="box__validationImg" src="/assets/images/close.png" alt="close">
                                <h6 class="box__validationText">Required Field</h6>
                            </div>
                            <div class="box__validation box__validation--invalidEmail">
                                <img class="box__validationImg" src="/assets/images/close.png" alt="close">
                                <h6 class="box__validationText">Type a valid email</h6>
                            </div>
                            <div class="box__validation box__validation--invalidEmailFound">
                                <img class="box__validationImg" src="/assets/images/close.png" alt="close">
                                <h6 class="box__validationText">Already registered, <a href="#" onclick="login()">Login</a></h6>
                            </div>
                            <div class="box__validation box__validation--valid">
                                <img class="box__validationImg" src="/assets/images/right.png" alt="close">
                                <h6 class="box__validationText">Looks great</h6>
                            </div>
                        </div>
                        <!-- USERNAME -->
                        <div class="box__input">
                            <input class="default-input" type="text" placeholder="Username" required>
                            <div class="box__validation box__validation--invalid">
                                <img class="box__validationImg" src="/assets/images/close.png" alt="close">
                                <h6 class="box__validationText">Required Field</h6>
                            </div>
                            <div class="box__validation box__validation--valid">
                                <img class="box__validationImg" src="/assets/images/right.png" alt="close">
                                <h6 class="box__validationText">Available</h6>
                            </div>
                        </div>
                        <!-- PASSWORD -->
                        <div class="box__input">
                            <input id="pass" class="default-input" matchTo="#confirmPass" type="password" placeholder="Passwrod" required>
                            <div class="box__validation box__validation--invalid">
                                <img class="box__validationImg" src="/assets/images/close.png" alt="close">
                                <h6 class="box__validationText">Required Field</h6>
                            </div>
                            <div class="box__validation box__validation--invalidPass">
                                <img class="box__validationImg" src="/assets/images/close.png" alt="close">
                                <h6 class="box__validationText">Passwords don't match</h6>
                            </div>
                            <div class="box__validation box__validation--valid">
                                <img class="box__validationImg" src="/assets/images/right.png" alt="close">
                                <h6 class="box__validationText">Looks great</h6>
                            </div>
                        </div>
                        <!-- REPEATE PASSWORD -->
                        <div class="box__input">
                            <input id="confirmPass" class="default-input" matchTo="#pass" type="password" placeholder="Confirm Passwrod" required>
                            <div class="box__validation box__validation--invalid">
                                <img class="box__validationImg" src="/assets/images/close.png" alt="close">
                                <h6 class="box__validationText">Required Field</h6>
                            </div>
                            <div class="box__validation box__validation--invalidPass">
                                <img class="box__validationImg" src="/assets/images/close.png" alt="close">
                                <h6 class="box__validationText">Passwords don't match</h6>
                            </div>
                            <div class="box__validation box__validation--valid">
                                <img class="box__validationImg" src="/assets/images/right.png" alt="close">
                                <h6 class="box__validationText">Password match</h6>
                            </div>
                        </div>
                        <!-- Gender -->
                        <div class="box__input">
                            <select id="gender" class="default-input" required>
                                <option value="" disabled selected>Select Sex</option>
                                <option value="male">Male</option>
                                <option value="female">Female</option>
                            </select>
                            <div class="box__validation box__validation--invalid">
                                <img class="box__validationImg" src="/assets/images/close.png" alt="close">
                                <h6 class="box__validationText">Required Field</h6>
                            </div>
                            <div class="box__validation box__validation--valid">
                                <img class="box__validationImg" src="/assets/images/right.png" alt="close">
                            </div>
                        </div>
                        <!-- SIGN UP -->
                        <div class="box__input">
                            <h5>
                                By clicking Register, you agree to our <a href="#">Terms</a>
                                and that you have read our <a href="#">Data Use Policy</a>,
                                including our <a href="#">Cookie Use.</a>
                            </h5>
                            <div class="f-right">
                                <button class="button button--login button--invalid" onclick="alert('Congrats, you registered successfully')">Register</button>
                            </div>
                        </div>
                    </form>`;
    $('.box').addClass('box--register');
    $('.box__data--form').empty().append(registerForm);
    validate('#registerForm');
}

function login() {
    let loginForm = `<form id="loginForm">
                        <!-- USERNAME -->
                        <div class="box__input box__input--invalid">
                            <input class="default-input" type="text" placeholder="Username" required>
                            <div class="box__validation box__validation--invalid">
                                <img class="box__validationImg" src="/assets/images/close.png" alt="close">
                                <h6 class="box__validationText">Required Field</h6>
                            </div>
                            <div class="box__validation box__validation--valid">
                                <img class="box__validationImg" src="/assets/images/right.png" alt="close">
                                <h6 class="box__validationText">Looks great</h6>
                            </div>
                        </div>
                        <!-- PASSWORD -->
                        <div class="box__input">
                            <input class="default-input" type="password" placeholder="Password" required>
                            <div class="box__validation box__validation--invalid">
                                <img class="box__validationImg" src="/assets/images/close.png" alt="close">
                                <h6 class="box__validationText">Required Field</h6>
                            </div>
                            <div class="box__validation box__validation--valid">
                                <img class="box__validationImg" src="/assets/images/right.png" alt="close">
                                <h6 class="box__validationText">Looks great</h6>
                            </div>
                        </div>
                        <!-- LOGIN -->
                        <div class="box__input">
                            <div class="f-left box__remember">
                                <input class="v-middle" id="remember" type="checkbox">
                                <label for="remember">Remember me</label>
                            </div>
                            <div class="f-right">
                                <button class="button button--login button--invalid" onclick="alert('You are logged in')"
                                        disabled="true">
                                    Login
                                </button>
                            </div>
                        </div>
                    </form>`

    $('.box').removeClass('box--register');
    $('.box__data--form').empty().append(loginForm);
    validate('#loginForm');
}