```
sass/
| 
|– components/ 
|   |– _buttons.scss     # Buttons 
|   |– _carousel.scss    # Carousel 
|   |– _dropdown.scss    # Dropdown 
|   |– _navigation.scss  # Navigation 
|   ...                  # Etc… 
|   |– main.scss         # import all files in this folder
| 
|– helpers/ 
|   |– _variables.scss   # Sass Variables 
|   |– _functions.scss   # Sass Functions 
|   |– _mixins.scss      # Sass Mixins 
|   |– _helpers.scss     # Class & placeholders helpers 
|   ...                  # Etc… 
|   |– main.scss         # import all files in this folder
| 
|– layout/ 
|   |– _grid.scss        # Grid system
|   |– _header.scss      # Header 
|   |– _footer.scss      # Footer 
|   |– _sidebar.scss     # Sidebar 
|   |– _forms.scss       # Forms 
|   ...                  # Etc… 
|   main.scss            # import all files in this folder
| 
|– pages/ 
|   |– _home.scss        # Home specific styles 
|   |– _contact.scss     # Contact specific styles 
|   ...                  # Etc…
|   |– main.scss         # import all files in this folder 
| 
| 
`– main.scss             # primary Sass file. import all main.scss in folders
```

#### reference link: https://www.sitepoint.com/architecture-sass-project/