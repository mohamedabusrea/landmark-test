# Landmark-test

### Link: http://landmark.surge.sh

### Install dependencies:

```bash
$ npm install
$ bower install
```

### To start the server:
- A local server will be created using browser-sync with auto refreshing whenever files are modified.

```bash
$ gulp serve
```

### To build for production:
- it will create a dist folder with minified HTML files. Also js and css files will be concatinated and minified.
- this version should be run on server because all paths is related to the root "/"

```bash
$ gulp build
```


---

## Tools
- Nunjucks: HTML Template engine
- SASS: CSS preprocessor + BEM (methodology for naming CSS) + OOCSS
- ES6
- Gulp: Task manager
- [WebStorm](https://www.jetbrains.com/webstorm/?fromMenu) : JavaScript IDE
- [Surge](https://surge.sh/) : Static web publishing

## Notes
- Email has 3 validations (required - right email format - already exists)
- popup opens a video and when you close it it will stop the video
- test@test.com is the email that is already registered before
- I didn't use plugins. I created the popup functionality & validations manually.